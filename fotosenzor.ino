/* ---------------------------------------------------------
 * Autor: Enver Podgorcevic
 * Mentor: vanr.prof.dr. Lejla Banjanovic-Mehmedovic
 * Predmet: Projektovanje sistema na cipu
 * Kreirano: 31.3.2019   
 * ---------------------------------------------------------
 * Kod koji treba operira radom fotosenzora preko Arduina,
 * a koji ce u ovisnosti od stanja senzora na digitalni pin
 * 13 da FPGA plocici salje odgovarajuci signal.
 * ---------------------------------------------------------
 */

/*
 * Funkcija koja se izvrsi jednom nakon predavanja kontrole
 * korisnickom kodu, i sluzi za inicijaliziranje varijabli
 * i pinova koji ce biti koristeni u glavnom dijelu koda.
 */
 
void setup() {
  pinMode(13, OUTPUT); //Setovanje digitalnog pina 13 u OUTPUT mode
  Serial.begin(9600); //Setovanje baud rate-a za UART komunikaciju izmedju arduina i racunara, koristi se za debugiranje
}

/*
 * Glavni dio koda koji se ponavlja sve dok Arduino ima napajanje.
 * Ispisuje analognu vrijednost sa senzora na Serial Monitor, a zatim
 * salje odgovarajuci digitalni signal na FPGA plocicu putem pina 13.
 */
 
void loop() {
  Serial.println(analogRead(A3)); //Ispis stanja senzora na racunar
  if(analogRead(A3) > 900) //Ispitivanje stanja senzora, se slanje odgovarajuceg digitalnog signala ka FPGA
    digitalWrite(13, HIGH);
  else
    digitalWrite(13, LOW);
  delay(100); //Delay od 100ms
}
