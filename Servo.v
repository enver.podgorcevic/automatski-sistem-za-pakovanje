/* ---------------------------------------------------------
 * Autor: Enver Podgorcevic
 * Mentor: vanr.prof.dr. Lejla Banjanovic-Mehmedovic
 * Predmet: Projektovanje sistema na cipu
 * Kreirano: 31.3.2019
 * ---------------------------------------------------------
 * Verilog kod koji treba operira logikom i radom aktuatora
 * projekta preko FPGA plocice.
 * ---------------------------------------------------------
 */

/*
 * Deklaracija modula i varijabli koje se biti koristene u kodu
 */
module servo ( clk, VAG, START, MASAx2, FOTO, TRAKA, VENTIL);

/*
 * Definiranje deskriptivnih konstanti koj ce se koristiti u 
 * radu sa servo motorima. Konstante su dobivene metodom opisanom
 * na vjezbama.
 */
parameter SERVO_FORWARD = 26'd85000;
parameter SERVO_STOP = 26'd75000;
parameter SERVO_BACKWARD = 26'd65000;

/*
 * Definiranje ulaza i izlaza sistema, kao i dodatnih registara
 * koji se biti potrebni za medjuproracune
 */
input wire clk, VAG, START, MASAx2, FOTO;		
output wire TRAKA, VENTIL;
reg [25:0] brojac_traka, brojac_ventil;
reg [25:0] impuls_traka, impuls_ventil;
reg izlaz_traka, izlaz_ventil;

/*
 * Glavni dio koda koji ce se izvrsavati na svaku pozitivnu ivicu
 * clock signala. Kako je logika rada sistema relativno jednostavna,
 * grananje toka programa nije implementirano pomocu FSM i tablice
 * tranzicija, vec pomocu if-else naredbi. Logika rada sistema data
 * je u seminarskom radu, pa detaljnije komentarisanje ovog dijela
 * koda nije potrebno.
 */
always@( posedge clk )
begin
	if(START == 1)
		begin
		if((VAG == 0) || (VAG == 1 && MASAx2 == 1))
			begin
			impuls_traka <= SERVO_FORWARD;
			if(FOTO == 1)
				begin
				impuls_ventil <= SERVO_FORWARD;
				end
			else
				begin
				impuls_ventil <= SERVO_BACKWARD;
				end
			end
		else if(VAG == 1)
			begin
			impuls_traka <= SERVO_STOP;
			end
		end
	else
		begin
		impuls_traka <= SERVO_STOP;
		impuls_ventil <= SERVO_STOP;
		end
/*
 * Dio koda koji operira servo motorom zaduzenim za pokretanje
 * pokretne trake. 
 */	
	if( brojac_traka < impuls_traka)
		begin
		izlaz_traka <= 1'b1;
		brojac_traka <= brojac_traka + 25'd1;
		end
	else if(brojac_traka >= impuls_traka && brojac_traka < impuls_traka + 25'd1000000)
		begin
		brojac_traka <= brojac_traka + 25'd1;
		izlaz_traka <= 1'b0;
		end
	else
		begin
		brojac_traka <= 25'd0;
		end

/*
 * Dio koda koji operira servo motorom zaduzenim za pokretanje
 * servo motora zaduzenog za otvaranje i zatvaranje ventila.
 */	
	if(brojac_ventil < impuls_ventil)
		begin
		izlaz_ventil <= 1'b1;
		brojac_ventil <= brojac_ventil + 25'd1;
		end
	else if(brojac_ventil >= impuls_ventil && brojac_ventil < impuls_ventil + 25'd1000000)
		begin
		brojac_ventil <= brojac_ventil + 25'd1;
		izlaz_ventil <= 1'b0;
		end
	else
		begin
		brojac_ventil <= 25'd0;
		end

end

/*
 * Dodjeljivanje prethodno preracunatih signala varijablama
 * asociranim sa servo motorima.
 */

assign TRAKA = izlaz_traka;
assign VENTIL = izlaz_ventil;
endmodule